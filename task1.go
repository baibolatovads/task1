package task1

import "math"

//func Test(s1 string, s2 string)string{
//	return s1 + s2
//}

func Test1(a float64, b float64, c float64)(float64, float64){
	//ax^2 + bx + c = 0
	//x(ax + b) = -c
	//x = -c  x = -b/a
	//(-b) +/- sqrt(b2 - 4ac)/2a
	res1 := -b + math.Sqrt(b * b - 4.0 * a * c)/(2*a)
	res2 := -b - math.Sqrt(math.Pow(b, 2) - 4 * a * c)/(2*a)
	return res1, res2
}